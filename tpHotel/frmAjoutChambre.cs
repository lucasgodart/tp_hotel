﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambre : Form
    {
        public frmAjoutChambre()
        {
            InitializeComponent();
        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAjoutChambre_Load(object sender, EventArgs e)
        {
            foreach(string s in Persistance.getListHotels())
            {
                cbxHotel.Items.Add(s);
            }
        }
    }
}
