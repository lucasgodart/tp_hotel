﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Hotel
    {
        // ATTRIBUTS
        private int id;
        private string nom;
        private string adresse;
        private string ville;

        // CONSTRUCTEUR
        public Hotel (int sonId, string sonNom, string sonAdresse, string saVille)
        {
            id = sonId;
            nom = sonNom;
            adresse = sonAdresse;
            ville = saVille;
        }

        public int Id { get => id; set => id = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Adresse { get => adresse; set => adresse = value; }
        public string Ville { get => ville; set => ville = value; }
    }
}
